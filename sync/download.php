<?php
 //Define location of required files in baltauto directory
require (dirname(__FILE__) . '/../includes/config.php');


//Change the PHP script timeout to infinite
set_time_limit(0);

//Increase the memory limit
ini_set('memory_limit', '-1');

$running_log = '';
$running_msg = array();
$newproducts = 0;
$updatedproducts = 0;
$lines = 0;
$query = array();
$progress = 0;
$error = 0;
$fileHasHeader = true;

$temp_table_name = $SUPPLIER_SHORT_NAME."_temp";


$time_start = microtime(true);



array_push($running_msg, "Starting...");
array_push($running_msg, "Downloading from FTP: $ftp_host");

// Download inventory file from FTP
$local_file = dirname(__FILE__) ."/../downloads/".$SUPPLIER_SHORT_NAME."feed-FULL-".date('m-d-Y-h.i.s').".zip";

if(true) {
		
				
			$ftp_connect = ftp_connect($ftp_host);
			$ftp_login = ftp_login($ftp_connect, $ftp_user, $ftp_password);

			ftp_pasv($ftp_connect, TRUE);

			//This function will kill the script if the feed file hasn't been updated since the last sync
			fileUpdated($ftp_connect, $ftp_filename, 1);

			if (ftp_get($ftp_connect, $local_file, $ftp_filename, FTP_BINARY)) {
				$running_log .= "\nSuccessfully downloaded update file to $local_file";
			} else {
				$running_log .= "\nFailed to download the file from the FTP";
				$message = "The script failed to download the update file.  The log below should show any errors that were captured:<br><br>".$running_log;
				mail($mailTo, "$SUPPLIER_NAME Script Failed", $message);    
				die($message);
			}

			ftp_close($ftp_connect); 

}
else $local_file = dirname(__FILE__) ."/../downloads/EDRESOURCEfeed-FULL-02-12-2020-12.12.36.zip";

array_push($running_msg, "Cleaning up downloads...");
unlink($local_file);
unlink($ftp_filename);

//DEBUG
print_r( $running_msg);


mysqli_close($link);
?>
